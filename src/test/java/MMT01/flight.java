package MMT01;

import java.util.ArrayList;
import java.util.List;
import java.util.*;
import javafx.util.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class flight 
{
	static WebDriver driver;
	public flight(WebDriver driver)
	{
		this.driver=driver;
	}
	
   public void setfrom(String f) throws InterruptedException
   {
	   driver.findElement(By.xpath("//div[@class='fsw_inputBox searchCity inactiveWidget ']")).click();
		driver.findElement(By.xpath(".//input[@placeholder='From']")).sendKeys(f);
		 Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//li[@id='react-autowhatever-1-section-0-item-0']")));
		driver.findElement(By.xpath(".//li[@id='react-autowhatever-1-section-0-item-0']")).click();
   }
   
   public void setto(String t) throws InterruptedException
   {

		driver.findElement(By.xpath(".//input[@placeholder='To']")).sendKeys(t);
		 Thread.sleep(3000);
		WebDriverWait wait2 = new WebDriverWait(driver, 30);
		WebElement element2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//li[@id='react-autowhatever-1-section-0-item-0']")));
		driver.findElement(By.xpath(".//li[@id='react-autowhatever-1-section-0-item-0']")).click();
   }
   
   public void setdate(String date)
   {
	  
		   try {
		   Thread.sleep(1000);
		   }
		   catch(Exception e) {
		   System.out.println("Exception");
		   }
		   String path=".//div[@aria-label='"+date+"']";
		   driver.findElement(By.xpath(path)).click();
   }
   
   public void clicksearch()
   {
		driver.findElement(By.xpath(".//a[@class=\"primaryBtn font24 latoBlack widgetSearchBtn \"]")).click();
   }
   
   public ArrayList <Pair <String,Integer> >  analyse ()
   {
	   ArrayList <Pair <String,Integer> > l = new ArrayList <Pair <String,Integer> > ();
	   try
		{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		        //js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		long lastHeight =  (Long) js.executeScript("return document.body.scrollHeight");

		    while (true) {
		        js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
		        Thread.sleep(2000);

		        long newHeight = (Long)js.executeScript("return document.body.scrollHeight");
		        if (newHeight == lastHeight) {
		            break;
		        }
		        lastHeight = newHeight;
		    }
		 List<WebElement> flights=driver.findElements(By.xpath(".//span[@class='airways-name ']"));

		 List<WebElement> price=driver.findElements(By.xpath(".//span[@class='actual-price']")); 
		 
		 
		 for(int i=0;i<flights.size();i++)
		 {
			 WebElement el1=flights.get(i);
			 WebElement el2=price.get(i);
			 
			 String[] rates=el2.getText().split(" ");
			 int rate=Utils.actual(rates[1]);
			 
			 l.add(new Pair <String,Integer> (el1.getText(), rate));
			 // System.out.println(rate);
		 }
		
		 
		}
	   catch(Exception  e)
		{
		System.out.println(e);
		}
	
		return l;
	   
   }
   
}
