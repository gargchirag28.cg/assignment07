package MMT01;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import javafx.util.Pair;

public class Test01
{
	WebDriver driver;
	@BeforeTest
	public void setup()
	{		
       WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.get("https://www.makemytrip.com");    
        
//       WebDriverManager.firefoxdriver().setup();
//       driver=new FirefoxDriver();
//       driver.get("https://www.makemytrip.com");
	}
	
	@Test
	public void test1() throws IOException, InterruptedException
	{
		flight f=new flight(driver);
		f.setfrom("Mumbai");
		f.setto("pune");
	    f.setdate("Wed Jan 29 2020");
	    f.clicksearch();
	    ArrayList <Pair <String,Integer> > list=f.analyse();
	    System.out.println("Total flights"+list.size());
	    
	    System.out.println(Utils.getmin(list, "IndiGo"));
	    System.out.println(Utils.getmax(list, "IndiGo"));
	    
	}
	
}
