package MMT01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javafx.util.Pair;

public class Utils 
{
    public static int actual (String n)
    {
       int r=0;
       
       for(int i=0;i<n.length();i++)
       {
    	   if(n.charAt(i)!=',')
    	   {
    		   r=r*10+n.charAt(i)-'0';
    	   }
       }
       
       return r;
    }
   
    public static  HashMap<String, ArrayList<Integer>> minmax( ArrayList <Pair <String,Integer> > list)
    {
    	//Pair<String,Integer>p=list[0];
    	HashMap<String, ArrayList<Integer>> val = new HashMap<String, ArrayList<Integer>>();
    	 for (Pair p: list)
    	 {
             String name=(String)p.getKey() ;
             Integer price= (Integer)p.getValue();
             
             
             
             
             
             for (int i = 0; i < list.size(); i++) 
             {
	     

		             if (!val.containsKey(name))
		             {
			             ArrayList<Integer> al = new ArrayList<Integer>();
			             al.add(price);
			             val.put(name, al);
		             }
		             else 
		             {
			             ArrayList<Integer> al = val.get(name);
			             al.add(price);
			             val.put(name, al);
		             }
		    	
                 }
    
             }
    	 return val;
    }
    
    
    public static int getmin( ArrayList <Pair <String,Integer> > list,String name)
    {
    	HashMap<String, ArrayList<Integer>> val =minmax(list);
    	
    	ArrayList<Integer> al = val.get(name);
    	Collections.sort(al);
    	return al.get(0);
    }
    public static int getmax(ArrayList <Pair <String,Integer> > list,String name)
    {
    	HashMap<String, ArrayList<Integer>> val =minmax(list);
    	
    	ArrayList<Integer> al = val.get(name);
    	Collections.sort(al);
    	return al.get(al.size() - 1);
    }
    
} 
